#!/bin/bash

export pluginsRoot="${ZSH_CUSTOM}/plugins"
export pluginRoot="${pluginsRoot}/beto"

if ! [ -d "${pluginRoot:?}" ]; then
    echo "${fg[red]:?}The BeTo plugin is not installed in the correct directory. Make sure it is installed in ${fg[yellow]:?}${pluginRoot:?}${reset_color:?}"
    return 1
fi

loadSources() {
    source "${pluginRoot:?}/_config"
    source "${pluginRoot:?}/aliases"

    source "${pluginRoot:?}/util/base64"
    source "${pluginRoot:?}/util/cd"
    source "${pluginRoot:?}/util/checkFunctionArguments"
    source "${pluginRoot:?}/util/other"
    source "${pluginRoot:?}/util/progressbar"

    source "${pluginRoot:?}/git/line-endings"

    source "${pluginRoot:?}/projects/setup"
    source "${pluginRoot:?}/projects/ssl"
    source "${pluginRoot:?}/projects/project-navigation"

    source "${pluginRoot:?}/other/keyboard"
}

if ! [ -f "${pluginRoot:?}/_config" ]; then
    echo "${fg[red]:?}The BeTo plugin for ZSH has not been setup yet.${reset_color:?}"
    echo "Run ${fg[yellow]:?}configureBeTo${reset_color:?} to configure."

    configureBeTo() {
        cp "${pluginRoot:?}/_config.example" "${pluginRoot:?}/_config"
        loadSources
        unset -f loadSources
    }

    return
fi

loadSources
unset -f loadSources
spr "$(basename "${PWD}")"
